# Requirements 

### ID: -- Identificador únic

RF-04-02

### Type/Category: -- Tipus i Subtipus

 Funcional

### Version: -- Número de versió

1.0

### Title: -- Nom del requisit

Rebuts i pagaments

### Description: -- Descripció del requisit

La plataforma al finalitzar cada mes, rebrà els pagaments de cada una de les botigues
pel transport i transferirà les quantitats de cada transportista en el seu compte
bancari.

### Relations: -- Requisits relacionats

* RF-04-01
* RNF-04-01

### Comments: -- Comentaris

Cap comentari
