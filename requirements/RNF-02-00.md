# Requirements RNF-02-00

### ID:
RNF-02-00

### Type/Category:
NO Funcional / Objectius de disseny

### Version:
1.0

### Title:
Objectiu de funcionament respecte al client

### Description:
L’objectiu de funcionament es concentrar-se en les necessitats del client

### Relations:
RNF-02-01

### Comments:
Cap comentari.

---
