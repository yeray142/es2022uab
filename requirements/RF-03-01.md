# Requirement RF-03-01

### ID:
RF-03-01

### Type/Category:
Funcional

### Version:
1.0

### Title:
Comissió directa per les comandes

### Description:
Per cada venda la plataforma carregarà una comissió directa per les comandes realitzades. Això consisteix a carregar un import determinat segons un variable pactat amb la botiga, de tal forma que la botiga no té despeses fixes per estar a la plataforma, i només paga pel seu volum de vendes.

### Relations:
_To be done (pending)_

### Comments:
Independentment dels abonaments possibles la plataforma sempre cobrarà la comissió a la botiga en el moment de fer-se el pagament.

---
