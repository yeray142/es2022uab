# Requirements RF-02-01

### ID:
RF-02-01

### Type/Category:
Funcional

### Version:
1.0

### Title:
Funcionament de les comandes

### Description:
Les comandes podran contenir un o més productes, amb quantitats diferents per a cada producte.
### Relations:
RNF-02-02

### Comments:
El client haurà d’especificar la quantitat (encara que pot ser d’una forma no concreta) en la seva petició; però totes les comandes contindran valors específics concrets de cada producte.

---
