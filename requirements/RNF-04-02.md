# Requirements 

### ID: -- Identificador únic

RNF-04-02

### Type/Category: -- Tipus i Subtipus

No funcional/ Decisions de Disseny

### Version: -- Número de versió

1.0

### Title: -- Nom del requisit

Numero comandes mateixa ruta

### Description: -- Descripció del requisit

Caldrà tenir en compte que un transportista podrà lliurar més d’una comanda en una mateixa ruta.


### Relations: -- Requisits relacionats

* RF-04-03

### Comments: -- Comentaris

Cap comentari