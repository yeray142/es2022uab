# Requirements RF-02-03

### ID:
RF-02-03

### Type/Category:
Funcional

### Version:
1.0

### Title:
Funcionament Selecció de la Comanda

### Description:
La selecció de la comanda i enviament succeeix quan l’usuari vol acceptar una de les ofertes rebudes. Llavors es genera una comanda en ferm i el botiguer rep totes les dades necessàries per a servir-la.

### Relations:
RF-02-00
RNF-02-03

### Comments:
Cap comentari.


---
