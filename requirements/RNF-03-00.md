# Requirement RNF-03-00

### ID:
RNF-03-00

### Type/Category:
NO funcional / Decisions de disseny

### Version:
1.0

### Title:
Assegurança amb companyia externa.

### Description:
Les reclamacions no seran tampoc automàtiques, però sí que estaran parcialment protegides per una assegurança amb una companyia externa.

### Relations:
* RF-03-03

### Comments:
Cap comentari

---
