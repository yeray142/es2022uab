# Requirements RNF-02-04

### ID:
RNF-02-04

### Type/Category:
NO Funcional / Restriccions de disseny

### Version:
1.0

### Title:
Restricció numero de comandes

### Description:
El màxim de comandes que un usuari podrà tenir en marxa, és a dir, en estat d’enviament és de 100.


### Relations:
Cap relació.

### Comments:
Cap comentari.


---
