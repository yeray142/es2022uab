# Requirement RNF-07-00

### ID:
RNF-07-00

### Type/Category:
NO funcional / Decisions de Disseny

### Version:
1.0

### Title:
SGBD MySQL

### Description:
El Sistema Gestor de Bases de Dades haurà de ser MySQL. Totes les dades del sistema es guardaran en una BDD Relacional. Caldrà proporcionar un conjunt de d'arxius en format SQL per definir les taules i restriccions de la Base de Dades

### Relations:


### Comments:
Caldrà definir en el disseny les versions especifiques a utilitzar i la plataforma a on s'executarà. També hi serà necessari definir un roadmap de manteniment amb un mínim de 6 anys. En el cas de que el EOL no pugui abarcar aquest període caldrà dissenyar un procés de migració.

---