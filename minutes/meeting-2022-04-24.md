# Meeting Minutes 24/04/2022

### DATE:
24 d'Abril del 2022 des de les 14:30 fins a les 15:00.

### Attendees:
* Yeray Cordero (NIU: 1599053)
* Adrian Vargas (NIU: 1606868)
* Marc Garcia (NIU: 1598865)

### Minutes of the meeting:
* Assignació de tasques pel segon sprint.
* Triat el _Scrum Master_ per al segon sprint, seguirà sent Yeray.
* Assignació de tasques al Azure DevOps.
* Preparació de les carpetes i les branques al Github.

---
