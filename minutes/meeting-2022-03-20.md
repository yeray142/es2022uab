# Meeting Minutes 20/03/2022

### DATE:
20 de Març del 2022 des de les 13:30 fins a les 13:45.

### Attendees:
* Yeray Cordero (NIU: 1599053)
* Adrian Vargas (NIU: 1606868)

### Minutes of the meeting:
* Solucionar problemes d'accés a l'Azure DevOps provocats per un error de Microsoft amb els permisos dels comptes.
* Assignar les tasques de la reunió anterior a l'Azure DevOps.

---
