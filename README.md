# Enginyeria del Software - 2022
### Grup
* Grup de matrícula: 411
* Subrgrup: 07

### Integrants
1. [Yeray Cordero, NIU: 1599053, Github user: yeray142](https://github.com/yeray142)
2. [Marc Garcia, NIU: 1598865, Github user: MarcGM04](https://github.com/MarcGM04)
3. [Adrian Vargas, NIU: 1606868, Github user: 1606868-uab](https://github.com/1606868-uab)

## Introducció
Aquest repositori conté les pràctiques que es fan a terme a Enginyeria del Software. Enginyeria Informàtica. Escola d'Enginyeria. Curs 2021-22. Universitat Autònoma de Barcelona.

## Contingut
1. [documents](https://github.com/yeray142/es2022uab/tree/master/documents)
2. [requirements](https://github.com/yeray142/es2022uab/tree/master/requirements)
3. [minutes](https://github.com/yeray142/es2022uab/tree/master/minutes)
4. [diagrams](https://github.com/yeray142/es2022uab/tree/master/diagrams)
5. [others](https://github.com/yeray142/es2022uab/tree/master/others)
6. [specifications](https://github.com/yeray142/es2022uab/tree/master/specifications)
7. [src](https://github.com/yeray142/es2022uab/tree/master/src)

## Sprint #1

### Objectius del lliurament
Els objectius d'aquest primer lliurament són els següents:
* Elaboració de la versió 1 del _Document d'Especificacions_.
* Elaboració del _Diagrama de casos d'ús_.
* Primera fase de _Seguiment i planificació_ del projecte.

### Explicació de l'entrega
Dintre de cada carpeta podeu trobar la següent informació/fitxers:
* Readme.md : Document en markdown descriptiu del projecte.
* /documents/ : Els documents lliurats.
* /requirements/ : Les fitxes de cadascun dels requisits.
* /minutes/ : Les actes de diferents reunions.
* /diagrams/ : Els arxius dels diagrames generats.
* /others/ : Altres arxius.

### Materials produits a l'entrega

#### Document d'Especificacions
Aquest document conté tota la següent informació:
* Llista dels integrats de l’equip de desenvolupament (i supervisor/s).
* Enllaços al projecte a Azure DevOps i GitHub.
* Llista formal de requisits organitzada per categories (inclou tota la informació de cada requisit copiada de l’arxiu markdown, més un enllaç directe a l’arxiu original).
* Llista dels perfils d’usuari identificats.
* Comentaris addicionals.

#### Diagrama de casos d'ús
S'identifiquen els actors, quins son els casos d'ús i quines relacions hi ha entre els actors i els casos d'ús.

## Sprint #2

### Objectius del lliurament
Els objectius d'aquest segon lliurament són els següents:
* Actualització del  _Document d'Especificacions_ a la versió 2.
* Elaboració del _Diagrama de casos d'activitats_.
* Segona fase de _Seguiment i planificació_ del projecte.

### Explicació de l'entrega
Dintre de cada carpeta podeu trobar la següent informació/fitxers:
* Readme.md : Document en markdown descriptiu del projecte.
* /documents/ : Els documents lliurats.
* /requirements/ : Les fitxes de cadascun dels requisits.
* /specifications/ : Les fitxes de cadascuna de les especificacions de cas d'ús.
* /minutes/ : Les actes de diferents reunions.
* /diagrams/ : Els arxius dels diagrames generats.
* /others/ : Altres arxius.

### Materials produits a l'entrega

#### Document d'Especificacions V2
Aquest document conté tota la següent informació:
* Informació generada al **Sprint #1**.
* Descripció de cadascuna de les especificacions de cas d’ús, més un enllaç al fitxer com a nota a peu de pàgina.
* Imatges dels diagrames d’activitats per cadascuna de les especificacions de cas d’ús, més un enllaç al fitxer (com a nota a peu de pàgina).
* Explicació de tots els diagrames i les relacions.

#### Especificacions de cas d'ús
Les especificacions de cas d'ús son uns arxius que indiquen amb més detall quins son els actors, pre-condicions i post-condicions així com el flux principal i subfluxos del sistema.
S'han fet especificacions dels casos d'ús: Solicitud comanda, Presentació ofertes, Selecció comanda.

#### Diagrames d'activitats
Son diagrames de fluxos d'accions que mostra l'activitat que esdevé al llarg del temps. Una activitat produeix alguna acció que produeix algun canvi en el sistema o retorna un valor.
S'han generat tres diagrames d'activitats corresponents a les tres especificacions anteriors.

## Sprint #3

### Objectius del lliurament
Els objectius d'aquest tercer lliurament són els següents:
* Actualització del  _Document d'Especificacions_ a la versió 3.
* Elaboració del _Diagrama de classes_ i dels _Diagrames de seqüència_.
* Tercera fase de _Seguiment i planificació_ del projecte.

### Explicació de l'entrega
Dintre de cada carpeta podeu trobar la següent informació/fitxers:
* Readme.md : Document en markdown descriptiu del projecte.
* /documents/ : Els documents lliurats.
* /requirements/ : Les fitxes de cadascun dels requisits.
* /specifications/ : Les fitxes de cadascuna de les especificacions de cas d'ús.
* /minutes/ : Les actes de diferents reunions.
* /diagrams/ : Els arxius dels diagrames generats.
* /others/ : Altres arxius.

### Materials produits a l'entrega

#### Document d'Especificacions V3
Aquest document conté tota la següent informació:
* Informació generada al **Sprint #1** i al **Sprint #2**.
* Imatges dels Diagrama de classes i Diagrames de seqüència. 
* Explicació de tots els diagrames i les relacions.

#### Diagrama de classes
El diagrama de classes descriu els tipus d'objectes d'un sistema i les relacions estàtiques que hi ha entre ells. Una classe és, doncs, una representació abstracta d'una categoria d'objectes. Un objecte seria, doncs, un concepte, abstracció o element amb límits definits, és a dir, una instància d'una classe.

#### Diagrames de seqüència
Aquests diagrames representen escenaris. Entenent escenari com una forma estructurada d'una història d'usuari o una descripció de la seqüència específica d'accions per il·lustrar el comportament d'un cas d'ús.
S'han generat tres diagrames de seqüència corresponents a les especificacions del **Sprint #2**.

## Sprint #4

### Objectius del lliurament
Els objectius d'aquest tercer lliurament són els següents:
* Utilitzar _Metodologia TDD_ per obtenir el disseny detallat associat a un cas d'ús concret.
* Actualització del _Diagrama de classes_ i dels _Diagrames de seqüència_ amb el disseny obtingut de l'execució de la metodologia TDD.
* Quarta fase de _Seguiment i planificació_ del projecte.
* Lliurament final del projecte.

### Explicació de l'entrega
Dintre de cada carpeta podeu trobar la següent informació/fitxers:
* Readme.md : Document en markdown descriptiu del projecte.
* /src/ : Els arxius de codi generat.
* /documents/ : Els documents lliurats i el fitxer ZIP amb el codi.
* /requirements/ : Les fitxes de cadascun dels requisits.
* /specifications/ : Les fitxes de cadascuna de les especificacions de cas d'ús.
* /minutes/ : Les actes de diferents reunions.
* /diagrams/ : Els arxius dels diagrames generats.
* /others/ : Altres arxius.

### Materials produits a l'entrega

#### Document d'Especificacions V4
Aquest document conté tota la següent informació:
* Informació generada al **Sprint #1**, al **Sprint #2** i al **Sprint #3**.
* Explicació de com s'ha realitzat el procés TDD.
* Justificació de les classes obtingudes.
* La versió 2 del diagrama de classes i la versió 2 del diagrama de seqüència del cas d'ús "Sol·licitud de Comanda".

#### Fitxer ZIP amb el codi
Un fitxer amb tot el codi Python (de producció i de test) necessaris per completar el conjunt de tests. 
